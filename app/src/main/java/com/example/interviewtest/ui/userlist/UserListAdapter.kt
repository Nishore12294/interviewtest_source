package example.com.interviewtest.view.ui.ui.homepage

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewtest.R
import com.example.interviewtest.ui.post.PostListActivity
import example.com.interviewtest.model.UsersList
import example.com.interviewtest.ui.userlist.UserListActivity


class UserListAdapter(private val list: List<UsersList>?) : RecyclerView.Adapter<UserViewHolder>() {
    var mContext: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        mContext = parent.context
        return UserViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val userData: UsersList? = list?.get(position)
        holder.bind(userData)
    }

    override fun getItemCount(): Int {
        if((list != null)){
            return list!!.size
        }else
            return 0

    }

}

class UserViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.user_list_item, parent, false)) {
    private var mNameView: TextView? = null
    private var mEmailIdView: TextView? = null
    private var mPhoneNoView: TextView? = null
    private var cardView: CardView? = null
    var mContext: Context? = null


    init {

        cardView = itemView.findViewById(R.id.card_userlist_item)
        mNameView = itemView.findViewById(R.id.userlist_title)
        mEmailIdView = itemView.findViewById(R.id.userlist_emailId)
        mPhoneNoView = itemView.findViewById(R.id.userlist_phoneno)
        mContext = parent.context
    }

    fun bind(usersList: UsersList?) {
        cardView!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, PostListActivity::class.java)
            val bundle = Bundle()
            bundle.putString(UserListActivity.Constants.USER_ID, ""+usersList!!.id)
            bundle.putString(UserListActivity.Constants.USER_NAME, ""+usersList!!.name)
            intent.putExtras(bundle)
            mContext!!.startActivity(intent)

        })
        mNameView?.text = " : "+usersList!!.name
        mEmailIdView?.text = " : "+usersList!!.email
        mPhoneNoView?.text = " : "+usersList!!.phone


    }

}