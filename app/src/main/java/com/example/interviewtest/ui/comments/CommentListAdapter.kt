package com.example.interviewtest.ui.comments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.cardview.widget.CardView
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewtest.CommentList
import com.example.interviewtest.R


class CommentListAdapter(private val list: List<CommentList>?) : RecyclerView.Adapter<CommentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return CommentViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val postData: CommentList? = list?.get(position)
        holder.bind(postData)
    }

    override fun getItemCount(): Int {
        if((list != null)){
            return list!!.size
        }else
            return 0

    }

}

class CommentViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.comment_list_item, parent, false)) {
    private var mCardView: CardView? = null
    private var mTitleView: TextView? = null
    private var mEmailIdView: TextView? = null
    private var mBodyView: TextView? = null
    var mContext: Context? = null

    init {
        mCardView = itemView.findViewById(R.id.card_commentlist_item)
        mTitleView = itemView.findViewById(R.id.commentlist_title)
        mEmailIdView = itemView.findViewById(R.id.commentlist_emailId)
        mBodyView = itemView.findViewById(R.id.commentlist_body)
        mContext = parent.context
    }

    fun bind(commentList: CommentList?) {
        mTitleView?.text = " : "+commentList!!.name
        mEmailIdView?.text = " : "+commentList!!.email
        mBodyView?.text = " : \n"+commentList!!.body
        mCardView!!.setOnClickListener(View.OnClickListener {


        })

    }

}