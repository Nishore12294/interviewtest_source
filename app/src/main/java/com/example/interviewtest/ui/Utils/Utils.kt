package com.example.interviewtest.ui.Utils

import android.content.Context
import android.net.ConnectivityManager


class Utils {

    companion object{
        fun isConnected(mContext:Context): Boolean {
            val connectivityManager =
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }
    }

}