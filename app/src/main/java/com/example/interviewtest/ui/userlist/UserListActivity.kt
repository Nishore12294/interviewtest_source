package example.com.interviewtest.ui.userlist

import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewtest.PhotoList
import com.example.interviewtest.R
import com.example.interviewtest.ui.Utils.Utils
import com.example.interviewtest.ui.webservices.APIClient
import com.example.interviewtest.ui.webservices.APInterface
import com.google.gson.Gson
import example.com.interviewtest.model.UsersList
import example.com.interviewtest.view.ui.ui.homepage.UserListAdapter
import kotlinx.android.synthetic.main.activity_user_list.*
import kotlinx.android.synthetic.main.progress_bar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.HttpURLConnection
import java.net.URL


class UserListActivity : AppCompatActivity() {
    var userList: List<UsersList>? = null
    object Constants {
        const val USER_ID = "USER_ID"
        const val USER_NAME = "USER_NAME"
    }
    companion object {
        private val defaultString: String? = null
        lateinit var mSharedPreferences: SharedPreferences
        val PHOTOLIST_KEY:String = "PHOTO_LIST"
        val SHARED_PRF:String = "photolist_sharedpreference"
    }
    private val layoutResId: Int
        @LayoutRes
        get() = R.layout.activity_user_list


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layoutResId)
        //API Call
        if(Utils.isConnected(this)) {
            callUserListApi()
        }else {
            Toast.makeText(
                applicationContext,
                applicationContext!!.getString(R.string.network_connection_problem),
                Toast.LENGTH_SHORT
            ).show()

        }

        AsyncTask.execute {
            //TODO your background code
            // PhotoAPICall1(this).execute()
            //PhotoAPICall(this).execute()

            mSharedPreferences = this@UserListActivity.getSharedPreferences(
                SHARED_PRF,
                Context.MODE_PRIVATE
            )
            var sharedPrefValue = mSharedPreferences.getString(PHOTOLIST_KEY, "")
            Log.e("sharedPrefValue", "get sharedPrefValue :==>" + sharedPrefValue)

            if(sharedPrefValue == null || sharedPrefValue == ""){
                if(Utils.isConnected(this)) {
                    callUserPhotoListApi()
                }else {
                    Toast.makeText(
                        applicationContext,
                        applicationContext!!.getString(R.string.network_connection_problem),
                        Toast.LENGTH_SHORT
                    ).show()

                }


            }

        }


        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = UserListAdapter(userList)
        }
        setTitle("Users List")
    }


    private fun callUserListApi() {
        progressBarNew.visibility = View.VISIBLE
        val apiInterfaces = APIClient.getClient().create(APInterface::class.java)
        val call = apiInterfaces.getUserList()
        call.enqueue(object : Callback<List<UsersList>> {
            override fun onResponse(
                call: Call<List<UsersList>>?,
                response: Response<List<UsersList>>?
            ) {
                progressBarNew.visibility = View.GONE
                if (response != null && response.isSuccessful) {
                    if (response.body() != null) {
                        userList = response.body()
                        list_recycler_view.apply {
                            layoutManager = LinearLayoutManager(applicationContext)
                            adapter = UserListAdapter(userList)
                        }

                        Log.e("Userlist", ":==>" + response.body().toString())
                        //Toast.makeText(applicationContext, applicationContext!!.getString(R.string.userlist_success), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            applicationContext!!.getString(R.string.somthing_went_wrong),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<UsersList>>?, t: Throwable?) {
                progressBarNew.visibility = View.GONE
                Log.e("Userlist", ":==>" + t!!.message.toString())
                Toast.makeText(
                    applicationContext,
                    applicationContext!!.getString(R.string.somthing_went_wrong),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }
    private fun callUserPhotoListApi() {
        progressBarNew.visibility = View.VISIBLE
        var result:List<PhotoList> ? = null;
        val apiInterfaces = APIClient.getClient().create(APInterface::class.java)
        val call = apiInterfaces.getPhotoList()
        call.enqueue(object : Callback<List<PhotoList>> {
            override fun onResponse(
                call: Call<List<PhotoList>>?,
                response: Response<List<PhotoList>>?
            ) {
                progressBarNew.visibility = View.GONE
                if (response != null && response.isSuccessful) {
                    if (response.body() != null) {
                        result = response.body()
                        storeListValue(this@UserListActivity, result)
                        Log.e(
                            "Photo list",
                            "doInBackground Photo Photo list :==>" + response.body()
                                .toString()
                        )

                    } else {
                    }
                }
            }

            override fun onFailure(call: Call<List<PhotoList>>?, t: Throwable?) {
                progressBarNew.visibility = View.GONE
                Log.e("Photo list", ":==>" + t!!.message.toString())
            }
        })

    }

    fun storeListValue(mContext: Context, listData: List<PhotoList>?) {
        mSharedPreferences = this@UserListActivity.getSharedPreferences(
            SHARED_PRF,
            Context.MODE_PRIVATE
        )
        val editor = mSharedPreferences!!.edit()
        var gson =  Gson()
        var jsonStr = gson.toJson(listData)
        editor.putString(PHOTOLIST_KEY, jsonStr)
        editor.commit()
    }






///////////////////
    /*  class PhotoAPICall(private var activity: UserListActivity?) : AsyncTask<String, String, String>() {

          override fun onPreExecute() {
              super.onPreExecute()
              //activity?.MyprogressBar?.visibility = View.VISIBLE
          }

          override fun doInBackground(vararg p0: String?): String {

              var result = ""
              try {
                  val url = URL("https://jsonplaceholder.typicode.com/photos")
                  val httpURLConnection = url.openConnection() as HttpURLConnection

                  httpURLConnection.readTimeout = 8000
                  httpURLConnection.connectTimeout = 8000
                  httpURLConnection.doOutput = true
                  httpURLConnection.connect()

                  val responseCode: Int = httpURLConnection.responseCode
                  Log.e("Response Code", " Response Code " + responseCode)

  //                if (responseCode == 200 || responseCode == 201 ) {
  //                    val inStream: InputStream = httpURLConnection.inputStream
  //                    val isReader = InputStreamReader(inStream)
  //                    val bReader = BufferedReader(isReader)
  //                    var tempStr: String?
  //
  //                    try {
  //
  //                        while (true) {
  //                            tempStr = bReader.readLine()
  //                            if (tempStr == null) {
  //                                break
  //                            }
  //                            result += tempStr
  //                        }
  //                        Log.e("Response ", "Response :==> " + result)
  //                    } catch (Ex: Exception) {
  //                        Log.e("Exception ", ":==> " + Ex.printStackTrace())
  //                    }
  //                }



              } catch (ex: Exception) {
                  Log.e("", "Error in doInBackground " + ex.message)
              }
              return result
          }

          @RequiresApi(Build.VERSION_CODES.KITKAT)
          override fun onPostExecute(result: String?) {
              super.onPostExecute(result)
             // activity?.MyprogressBar?.visibility = View.INVISIBLE
              if (result == "") {

                  Toast.makeText(
                      activity,
                      "Photos API " + activity?.getString(R.string.somthing_went_wrong),
                      Toast.LENGTH_SHORT
                  ).show()
              } else {
                  try {
                      var parsedResult = ""
                    //  var jsonObject: JSONObject? = JSONObject(result)
                     // Log.d("onPostExecute", " jsonObject response :==>" + jsonObject.toString())

  //                    val jsonArr = JSONArray(result)
  //                    Log.e("onPostExecute", " jsonArr response :==>" + jsonArr.toString())
  //                    for (i in 0 until jsonArr.length()) {
  //                        val jsonObj = jsonArr.getJSONObject(i)
  //                        println(jsonObj)
  //                    }

                      //val jsonObject = JSONObject(result)
                      //val names: Array<String> = JSONObject.getNames(jsonObject)
                     // val jsonArray = jsonObject.toJSONArray()
                      //val mapper = ObjectMapper()

                      Log.e("", "onPostExecute " + result.toString())
                      val gson = Gson()
                      val objectList = gson.fromJson(result, Array<PhotoList>::class.java).asList()

                      Log.e("", "onPostExecute " + objectList.toString())

  //                    var participantJsonList: List<PhotoList> =
  //                        mapper.readValue(jsonString, object : TypeReference<List<Student?>?>() {})
                  }catch (ex: java.lang.Exception){
                      Log.e("", "Error in doInBackground " + ex.message)
                  }

              }
          }
      }

      class PhotoAPICall1(private var context: Context?) : AsyncTask<String, String?, List<PhotoList>?>() {
  //        companion object {
  //            private val defaultString: String? = null
  //            lateinit var mSharedPreferences: SharedPreferences
  //            val photolist:String = "PHOTO_LIST"
  //            val SHARED_PRF:String = "photolist_sharedpreference"
  //        }
  //        val editor = mSharedPreferences!!.edit()
  //        var gson =  Gson()
  //        var jsonStr = null


          override fun onPreExecute() {
              super.onPreExecute()
              //mSharedPreferences = context!!.getSharedPreferences(SHARED_PRF, Context.MODE_PRIVATE)

          }

          override fun doInBackground(vararg p0: String?): List<PhotoList>? {

              var result: List<PhotoList>? = null
              try {

              } catch (ex: Exception) {
                  Log.e("", "Error in doInBackground " + ex.message)
                  result = null
              }
              return result
          }

          @RequiresApi(Build.VERSION_CODES.KITKAT)
          override fun onPostExecute(result: List<PhotoList>?) {
              super.onPostExecute(result)
              if (result != null) {
                  //storeListValue(activity,result)
                  Log.e("Photo list", "onPostExecute Photo list :==>" + result)
              } else {
                  Log.e("Photo list", "onPostExecute result is empty")
              }


          }



  //        fun storeListValue(mContext: Context,value: List<T>) {
  //            mSharedPreferences = mContext.getSharedPreferences("photolist_sharedpreference", Context.MODE_PRIVATE)
  //            val editor = mSharedPreferences!!.edit()
  //            var gson =  Gson()
  //             var jsonStr = gson.toJson(value)
  //            editor.putString("photolist", jsonStr)
  //            editor.commit()
  //        }

      }

  */

}
