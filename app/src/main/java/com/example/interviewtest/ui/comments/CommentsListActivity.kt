package com.example.interviewtest.ui.comments

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewtest.CommentList
import com.example.interviewtest.R
import com.example.interviewtest.ui.Utils.Utils
import com.example.interviewtest.ui.post.PostListActivity
import com.example.interviewtest.ui.webservices.APIClient
import com.example.interviewtest.ui.webservices.APInterface
import kotlinx.android.synthetic.main.activity_comments_list.*
import kotlinx.android.synthetic.main.progress_bar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CommentsListActivity : AppCompatActivity() {
    var commentList: List<CommentList>? = null

    private val layoutResId: Int
        @LayoutRes
        get() = R.layout.activity_comments_list


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layoutResId)
        //Get the bundle
        val bundle = intent.extras
        val postId = bundle!!.getString(PostListActivity.Constants.POSTID)
        Log.e("Post Id",":==>"+postId)

        //API Call
        if(Utils.isConnected(this)) {
            callCommentListApi(postId)
        }else {
            Toast.makeText(
                applicationContext,
                applicationContext!!.getString(R.string.network_connection_problem),
                Toast.LENGTH_SHORT
            ).show()

        }

        comment_recycler_view.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = CommentListAdapter(commentList)
        }

        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        setTitle("Comments List")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return true
    }
    private fun callCommentListApi(postId:String?) {
        progressBarNew.visibility = View.VISIBLE
        val apiInterfaces = APIClient.getClient().create(APInterface::class.java)
        val call = apiInterfaces.getCommentsList(postId)
        call.enqueue(object : Callback<List<CommentList>> {
            override fun onResponse(
                call: Call<List<CommentList>>?,
                response: Response<List<CommentList>>?
            ) {
                progressBarNew.visibility = View.GONE
                if (response != null && response.isSuccessful) {

                    if (response.body() != null) {
                        commentList = response.body()
                        comment_recycler_view.apply {
                            layoutManager = LinearLayoutManager(applicationContext)
                            adapter = CommentListAdapter(commentList)
                        }

                        Log.e("CommentList", ":==>" + response.body().toString())
//                        Toast.makeText(
//                            applicationContext,
//                            applicationContext!!.getString(R.string.userlist_success),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            applicationContext!!.getString(R.string.somthing_went_wrong),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<CommentList>>?, t: Throwable?) {
                progressBarNew.visibility = View.GONE
                Log.e("CommentList", ":==>" + t!!.message.toString())
                Toast.makeText(
                    applicationContext,
                    applicationContext!!.getString(R.string.somthing_went_wrong),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }
}