package com.example.interviewtest.ui.post

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewtest.PhotoList
import com.example.interviewtest.R
import com.example.interviewtest.model.PostList
import com.example.interviewtest.ui.comments.CommentsListActivity
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


class PostListAdapter(
    private val userName: String?,
    private val list: List<PostList>?,
    private val userPhotoList: List<PhotoList>?
) : RecyclerView.Adapter<UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return UserViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val postData: PostList? = list?.get(position)
        val photoData: PhotoList? = userPhotoList?.get(position)
        val userName: String? = userName
        holder.bind(userName, postData, photoData)
    }

    override fun getItemCount(): Int {
        if((list != null)){
            return list!!.size
        }else
            return 0

    }

}

class UserViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.post_list_item, parent, false)) {
    private var mCommentBtn: Button? = null
    private var mImageView: ImageView? = null
    private var mPostUserNameView: TextView? = null
    private var mPostTitleView: TextView? = null
    private var mPostBodyView: TextView? = null
    var mContext: Context? = null

    init {
        mCommentBtn = itemView.findViewById(R.id.comment_btn)
        mImageView = itemView.findViewById(R.id.postImage)
        mPostUserNameView = itemView.findViewById(R.id.postlist_username)
        mPostTitleView = itemView.findViewById(R.id.postlist_title)
        mPostBodyView = itemView.findViewById(R.id.postlist_body)
        mContext = parent.context
    }

    fun bind(userName: String?, postList: PostList?, photoList: PhotoList?) {
        mPostUserNameView?.text = ""+userName
        mPostTitleView?.text = ""+postList!!.title
        mPostBodyView?.text = ""+postList!!.body

        Log.e("photoList", "photoList Image :==>" + photoList!!.thumbnailUrl)
        Log.e("postList", "postList User Image :==>" + postList.userImage)

        mContext?.let {
            Picasso.with(it)
                .load(postList.userImage)
                .placeholder(R.drawable.sample_poster1)
               // .resize(30, 30)
                .into(mImageView)
        };

        mCommentBtn!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, CommentsListActivity::class.java)
            val bundle = Bundle()
            bundle.putString(PostListActivity.Constants.POSTID, "" + postList!!.id)
            intent.putExtras(bundle)
            mContext!!.startActivity(intent)
        })

    }

}