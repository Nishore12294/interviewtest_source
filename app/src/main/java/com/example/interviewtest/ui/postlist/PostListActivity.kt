package com.example.interviewtest.ui.post

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewtest.PhotoList
import com.example.interviewtest.R
import com.example.interviewtest.model.PostList
import com.example.interviewtest.ui.Utils.Utils
import com.example.interviewtest.ui.webservices.APIClient
import com.example.interviewtest.ui.webservices.APInterface
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import example.com.interviewtest.ui.userlist.UserListActivity
import kotlinx.android.synthetic.main.activity_post_list.*
import kotlinx.android.synthetic.main.progress_bar.*
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type


class PostListActivity : AppCompatActivity() {
    var postList: List<PostList>? = null
    var photoList:List<PhotoList>? = null
    object Constants {
        const val POSTID = "POST_ID"
    }
    private val layoutResId: Int
        @LayoutRes
        get() = R.layout.activity_post_list


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layoutResId)
        //Get the bundle
        //Get the bundle
        val bundle = intent.extras
        val userId = bundle!!.getString(UserListActivity.Constants.USER_ID)
        val userName = bundle!!.getString(UserListActivity.Constants.USER_NAME)
        Log.e("userid", ":==>" + userId)
        Log.e("userName", ":==>" + userName)



        //Get Photo data and Call API
        getUserPhoto(userName,userId)
        postlist_recycler_view.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = PostListAdapter(userName,postList,photoList)
        }
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        setTitle("Posts List")

    }

    fun getUserPhoto(userName: String?,userId: String?){
        var sharedPrefValue = UserListActivity.mSharedPreferences.getString(
            UserListActivity.PHOTOLIST_KEY,
            ""
        )
        Log.e("sharedPrefValue", "get sharedPrefValue :==>" + sharedPrefValue)
        if(sharedPrefValue != "") {
            var jsonArr: JSONArray = JSONArray(sharedPrefValue);
            Log.e("JsonArr", "Json Arr :==>" + jsonArr.toString())
            val gson = GsonBuilder().create()
            photoList = gson.fromJson(jsonArr.toString(), Array<PhotoList>::class.java).toList()
            Log.e(
                "sharedPrefValue",
                "sharedPrefValue 1st value userId:==>" + photoList?.get(0)!!.id
            );
            Log.e(
                "sharedPrefValue",
                "sharedPrefValue 1st value body:==>" + photoList?.get(0)!!.thumbnailUrl
            );

        }
        if(Utils.isConnected(this)) {
            callPostListApi(userName, userId, photoList)
        }else {
            Toast.makeText(
                applicationContext,
                applicationContext!!.getString(R.string.network_connection_problem),
                Toast.LENGTH_SHORT
            ).show()

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return true
    }
    private fun callPostListApi(userName: String?,userId: String?,photoList:List<PhotoList>?) {
        progressBarNew.visibility = View.VISIBLE
        val apiInterfaces = APIClient.getClient().create(APInterface::class.java)
        val call = apiInterfaces.getPostList(userId)
        call.enqueue(object : Callback<List<PostList>> {
            override fun onResponse(
                call: Call<List<PostList>>?,
                response: Response<List<PostList>>?
            ) {
                progressBarNew.visibility = View.GONE
                if (response != null && response.isSuccessful) {
                    if (response.body() != null) {
                        postList = response.body()
                        if(postList != null && photoList != null)
                        for (postItem in postList!!) {
                            for(photoItem in photoList!!) {
                                if (postItem.id == photoItem.id) {
                                    Log.e("postItem","photoItem.thumbnailUrl:==>"+ photoItem.thumbnailUrl)
                                    postItem.userImage = photoItem.url
                                }
                            }
                        }



                        postlist_recycler_view.apply {
                            layoutManager = LinearLayoutManager(applicationContext)
                            adapter = PostListAdapter(userName,postList,photoList)
                        }

                        Log.e("Postlist", ":==>" + response.body().toString())
//                        Toast.makeText(
//                            applicationContext,
//                            applicationContext!!.getString(R.string.userlist_success),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            applicationContext!!.getString(R.string.somthing_went_wrong),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<PostList>>?, t: Throwable?) {
                progressBarNew.visibility = View.GONE
                Log.e("Postlist", ":==>" + t!!.message.toString())
                Toast.makeText(
                    applicationContext,
                    applicationContext!!.getString(R.string.somthing_went_wrong),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }
}

