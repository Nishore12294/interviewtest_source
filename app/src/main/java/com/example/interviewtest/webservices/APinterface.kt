package com.example.interviewtest.ui.webservices


import com.example.interviewtest.CommentList
import com.example.interviewtest.PhotoList
import com.example.interviewtest.model.PostList
import example.com.interviewtest.model.UsersList
import retrofit2.Call
import retrofit2.http.*


interface APInterface {

    @GET("/users")
    fun getUserList():
            Call<List<UsersList>>

    @GET("/posts?")
    fun getPostList( @Query("userId") userId: String?):Call<List<PostList>>

    @GET("/comments")
    fun getCommentsList(@Query("postId") postId: String?):
            Call<List<CommentList>>

    @GET("/photos")
    fun getPhotoList():
            Call<List<PhotoList>>

}