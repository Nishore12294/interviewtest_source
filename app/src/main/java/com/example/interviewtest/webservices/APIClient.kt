package com.example.interviewtest.ui.webservices

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class APIClient {

    companion object {
        val BASE_URL = "https://jsonplaceholder.typicode.com/"
        private var retrofit: Retrofit? = null
        var client = OkHttpClient()

        fun getClient(): Retrofit {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            client = OkHttpClient.Builder().connectTimeout(240, TimeUnit.SECONDS)
                .readTimeout(240, TimeUnit.SECONDS).addInterceptor(interceptor).build()
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit!!
        }

        fun getClientBg(): Retrofit {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            client = OkHttpClient.Builder().connectTimeout(240, TimeUnit.SECONDS)
                .readTimeout(240, TimeUnit.SECONDS).addInterceptor(interceptor).build()

            retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                .callbackExecutor(Executors.newSingleThreadExecutor()) // other builder options...
                .build()


            return retrofit!!
        }

    }
}
